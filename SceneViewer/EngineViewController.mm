//
//  EngineViewController.m
//  EngineIOSTest
//
//  Created by Tobias Boogh on 4/20/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//


#import "EngineViewController.h"
#include <Engine/EngineCommon.hpp>
#include <Engine/Render/OpenGLES20IOSExternal.h>
@interface EngineViewController (){
    unsigned long int frameNumber;
    
}

@property (nonatomic, strong) EAGLContext *context;
@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic) BOOL isAnimating;
-(void)drawFrame;
-(void)startAnimating;
-(void)stopAnimating;

-(void)HostRenderInit;
-(void)HostRenderShutdown;
@end

EngineViewController *engineViewController;

@implementation EngineViewController

void tin::ExternalHostInit(){
    [engineViewController HostRenderInit];
}

void tin::ExternalHostRenderShutdown(){
    [engineViewController HostRenderShutdown];
}

void tin::SetCurrentContext(){
    [engineViewController setCurrentContext];
}

void tin::PresentFrameBuffer(){
    [engineViewController presentFrameBuffer];
}

-(void)setCurrentContext{
    [EAGLContext setCurrentContext:_context];
}

-(void)presentFrameBuffer{
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)HostRenderInit{
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.view.layer];
}

-(void)HostRenderShutdown{
    _context = nil;
}

-(void)closeScene{
    sceneViewer->CloseScene("");
}

-(void)loadSceneNamed:(NSString *)sceneName{
    sceneViewer->LoadScene([sceneName cStringUsingEncoding:NSUTF8StringEncoding]);
}

-(void)viewDidLoad{
    NSString *tempDirectory = [@"~/tmp/" stringByExpandingTildeInPath];
    NSString *dataDirectory = [tempDirectory stringByAppendingPathComponent:@"data"];
    NSFileManager *filemanager = [[NSFileManager alloc] init];
    NSError *error = nil;
    if ([filemanager createDirectoryAtPath:dataDirectory withIntermediateDirectories:YES attributes:nil error:&error]){
        NSLog(@"%@", [error description]);
    }

    NSDirectoryEnumerator *directoryEnumerator = [filemanager enumeratorAtPath:[[NSBundle mainBundle] resourcePath]];
    NSString *filename = nil;
    while (filename = [directoryEnumerator nextObject]){
        if ([[filename pathExtension] isEqualToString:@"vsh"] || [[filename pathExtension] isEqualToString:@"fsh"]){
            NSError *error = nil;
            NSString *completeFilePath = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
            NSString *destinationPath = [dataDirectory stringByAppendingPathComponent:filename];
            // If a shader exists remove it
            if ([filemanager fileExistsAtPath:destinationPath]){
                [filemanager removeItemAtPath:destinationPath error:&error];
                if (error){
                    NSLog(@"%@", [error description]);
                }
            }
            error = nil;
            [filemanager copyItemAtPath:completeFilePath toPath:destinationPath error:&error];
            if (error){
                NSLog(@"%@", [error description]);
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{
    engineViewController = self;
    sceneViewer = new tin::SceneViewerIOS();
    
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:nil];
    [EAGLContext setCurrentContext:_context];
    
    sceneViewer->Start();
    _inputArray = [[NSMutableArray alloc] initWithCapacity:1];
    
    NSFileManager *filemanager = [[NSFileManager alloc] init];
    NSDirectoryEnumerator *dir_enum = [filemanager enumeratorAtPath:[@"~/Documents" stringByExpandingTildeInPath]];
    NSString *fileName;
    while (fileName = [dir_enum nextObject]){
        NSString *fileExtension = [fileName pathExtension];
        if ([fileExtension isEqualToString:@"tinPak"]){
            NSString *completePath = [[@"~/Documents" stringByExpandingTildeInPath] stringByAppendingPathComponent:fileName];
            sceneViewer->AddVirtualFileSystem([completePath cStringUsingEncoding:NSUTF8StringEncoding]);
        }
    }
    
    if (_sceneName){
        [self loadSceneNamed:_sceneName];
    }
//    [self startAnimating];
    
    if (self.navigationController){
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Trash" style:UIBarButtonItemStylePlain target:self action:@selector(testShutdown:)];
        self.navigationItem.rightBarButtonItem = item;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    // No more calls for frames
    // let the engine release resources gracefully
    delete sceneViewer;
    [self stopAnimating]; // Need one last frame to clear resources
    _context = nil;
    // destroy the engine
}

-(NSArray *)getSceneNames{
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
    std::vector<std::string> scene_files = sceneViewer->SceneFilenames();
    for (auto &filename : scene_files){
        [array addObject:[NSString stringWithCString:filename.c_str() encoding:NSUTF8StringEncoding]];
    }
    return array;
}

-(void)drawFrame{
    if (_isAnimating){
//        [EAGLContext setCurrentContext:_context];
//        sceneViewer->Frame();
//        [_context presentRenderbuffer:GL_RENDERBUFFER];
    }
}

-(void)startAnimating{
    [self stopAnimating];
    if (!_isAnimating){
        _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(drawFrame)];
        [_displayLink setFrameInterval:2.0];
        [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        _isAnimating = YES;
    }
}

-(void)stopAnimating{
    if (_isAnimating){
        [_displayLink invalidate];
        _isAnimating = NO;
    }
}

tin::InputInfo tin::GetBufferedInput(){
    tin::InputInfo input_info;
    if ([[engineViewController inputArray] count] == 0){
        input_info.type = tin::kInputTypeEnd;
    } else {
        NSValue *value = [[engineViewController inputArray] objectAtIndex:0];
        [value getValue:&input_info];
        [[engineViewController inputArray] removeObjectAtIndex:0];
    }
    return input_info;
}

-(void)touchDownAtPoint:(CGPoint)point{
    tin::InputInfo input_info;
    input_info.type = tin::InputType::kInputTypeTouch;
    input_info.touch.phase = tin::TouchPhase::kTouchPhaseBegin;
    input_info.touch.position.x = point.x;
    input_info.touch.position.y = point.y;
    [_inputArray addObject:[NSValue value:&input_info withObjCType:@encode(tin::InputInfo)]];
}

-(void)touchUpAtPoint:(CGPoint)point{
    tin::InputInfo input_info;
    input_info.type = tin::InputType::kInputTypeTouch;
    input_info.touch.phase = tin::TouchPhase::kTouchPhaseEnd;
    input_info.touch.position.x = point.x;
    input_info.touch.position.y = point.y;
    [_inputArray addObject:[NSValue value:&input_info withObjCType:@encode(tin::InputInfo)]];
}

-(void)touchMovedAtPoint:(CGPoint)point withPreviousPoint:(CGPoint)previous{
    tin::InputInfo input_info;
    input_info.type = tin::InputType::kInputTypeTouch;
    input_info.value1 = 2; // Move
    input_info.touch.phase = tin::TouchPhase::kTouchPhaseMoved;
    input_info.touch.position.x = point.x;
    input_info.touch.position.y = point.y;
    input_info.touch.previous_position.x = previous.x;
    input_info.touch.previous_position.y = previous.y;
//    NSLog(@"%f, %f", previous.x - point.x, previous.y - point.y);
    [_inputArray addObject:[NSValue value:&input_info withObjCType:@encode(tin::InputInfo)]];
}

-(void)viewDidLayoutSubviews{
    CGRect frame = self.view.frame;
    CGRect bounds = self.view.bounds;
    // This direct command should be replaced with a command system in the next iteration.
    sceneViewer->SetViewPort(0, 0, bounds.size.width, bounds.size.height);
}

void LogNSMessage(const char *message){
    NSLog(@"%s", message);
}

void LogNSWarning(const char *message){
    NSLog(@"%s", message);
}

void LogNSError(const char *message){
    NSLog(@"%s", message);
}

-(void)didReceiveMemoryWarning{
    NSLog(@"[%@ didReceiveMemoryWarning]", NSStringFromClass([self class]));
    // Engine should purge resources
    [super didReceiveMemoryWarning];
    if ([self.view window] == nil){
//        engine->Shutdown();
        self.view = nil;
    }
}
@end
