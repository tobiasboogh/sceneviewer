//
//  main.m
//  EngineIOSBase
//
//  Created by Tobias Boogh on 12-07-27.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
