//
//  SceneFilesViewController.h
//  SceneViewer
//
//  Created by Tobias Boogh on 9/21/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneFilesViewController : UITableViewController <UISplitViewControllerDelegate>
-(IBAction)refreshList:(id)sender;
-(IBAction)closeScene:(id)sender;
@end
