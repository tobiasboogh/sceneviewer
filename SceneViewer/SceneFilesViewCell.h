//
//  SceneFilesViewCell.h
//  SceneViewer
//
//  Created by Tobias Boogh on 9/21/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneFilesViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, weak) IBOutlet UILabel *fileLabel;
@end
