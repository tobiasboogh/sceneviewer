//
//  EngineViewController.h
//  EngineIOSTest
//
//  Created by Tobias Boogh on 4/20/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "EngineView.h"
#import "SceneViewerIOS.h"
@interface EngineViewController : UIViewController <EngineInputDelegate>{
    tin::SceneViewerIOS *sceneViewer;
}
@property (nonatomic, copy) NSString *sceneName;
@property (nonatomic, strong) NSMutableArray *inputArray;

-(void)loadSceneNamed:(NSString *)sceneName;
-(void)closeScene;
-(NSArray *)getSceneNames;
@end
