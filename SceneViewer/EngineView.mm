//
//  EngineView.m
//  EngineIOSTest
//
//  Created by Tobias Boogh on 4/20/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//

#import "EngineView.h"
#include <list>

@interface EngineView()

@end

@implementation EngineView
@synthesize inputDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.multipleTouchEnabled = YES;

    }
    return self;
}

+(Class)layerClass{
    return [CAEAGLLayer class];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches){
        [inputDelegate touchDownAtPoint:[touch locationInView:self]];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches){
        [inputDelegate touchUpAtPoint:[touch locationInView:self]];
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches){
        [inputDelegate touchMovedAtPoint:[touch locationInView:self] withPreviousPoint:[touch previousLocationInView:self]];
    }
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches){
        [inputDelegate touchUpAtPoint:[touch locationInView:self]];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
