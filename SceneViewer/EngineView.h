//
//  EngineView.h
//  EngineIOSTest
//
//  Created by Tobias Boogh on 4/20/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <QuartzCore/QuartzCore.h>
@protocol EngineInputDelegate <NSObject>
-(void)touchDownAtPoint:(CGPoint)point;
-(void)touchUpAtPoint:(CGPoint)point;
-(void)touchMovedAtPoint:(CGPoint)point withPreviousPoint:(CGPoint)previous;
@end

@interface EngineView : UIView
@property (nonatomic, weak) IBOutlet id <EngineInputDelegate> inputDelegate;
@end
