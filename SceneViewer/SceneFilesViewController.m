//
//  SceneFilesViewController.m
//  SceneViewer
//
//  Created by Tobias Boogh on 9/21/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import "SceneFilesViewController.h"
#import "SceneViewerModel.h"
#import "EngineViewController.h"
#import "SceneFilesViewCell.h"
@interface SceneFilesViewController ()
@property (nonatomic, strong) NSArray* filenameArray;
@property (nonatomic, strong) EngineViewController *engineViewController;
@property (nonatomic, strong) UIPopoverController *masterPopoverController;
@end

@implementation SceneFilesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        self.clearsSelectionOnViewWillAppear = NO;
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
        _engineViewController = (EngineViewController *)[self.splitViewController.viewControllers lastObject];
    } else {
        _engineViewController = nil;
    }
    
//    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
//    [refreshControl addTarget:self action:@selector(refreshList:) forControlEvents:UIControlEventValueChanged];
//    self.refreshControl = refreshControl;
    
    UINib *nib = [UINib nibWithNibName:@"SceneViewerViewCell" bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"sceneFileCellIdentifier"];
}

-(void)viewWillAppear:(BOOL)animated{
    [self refreshList:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _filenameArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"sceneFileCellIdentifier";
    SceneFilesViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.fileLabel.text = [_filenameArray objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    SceneFilesViewCell *cell = (SceneFilesViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    [cell.indicator startAnimating];
//    cell.indicator.hidden = NO;    
//    SceneViewerModel *model = [SceneViewerModel getInstance];
//    [model unzipResourceNamed:[_filenameArray objectAtIndex:indexPath.row] withCompletionBlock:^(BOOL success, NSArray *filenames) {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            cell.indicator.hidden = YES;
//            if (filenames.count > 0){
//                NSString *scene = [filenames objectAtIndex:0];
//                if (_engineViewController){
//                    [_engineViewController loadSceneNamed:scene];
//                } else {
//                    EngineViewController *engineController = [self.storyboard instantiateViewControllerWithIdentifier:@"EngineViewController"];
//                    engineController.sceneName = scene;
//                    [self.navigationController pushViewController:engineController animated:YES];
//                }
//            }
//        });
//    }];

    [_engineViewController loadSceneNamed:[_filenameArray objectAtIndex:indexPath.row]];
}

#pragma mark - Actions

-(IBAction)refreshList:(id)sender{
//    [self.refreshControl beginRefreshing];
    
//    SceneViewerModel *model = [SceneViewerModel getInstance];
//    [model refresh:^(BOOL success, NSArray *filenames) {
        _filenameArray = [self.engineViewController getSceneNames];
//        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
//    }];
}

-(IBAction)closeScene:(id)sender{
    if (_engineViewController){
        [_engineViewController closeScene];
    }
}
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}
-(BOOL)shouldAutorotate{
    return YES;
}

#pragma mark - SplitViewDelegate

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}


@end
