//
//  SceneViewer.cpp
//  Game
//
//  Created by Tobias Boogh on 12-09-06.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#include "SceneViewerIOS.h"
#include "tinNSLoggerInterface.h"
#include <Engine/EngineCommon.hpp>

namespace tin {
    SceneViewerIOS::~SceneViewerIOS(){
        tin::Engine::SharedInstance()->Destroy();
    }
    
    void SceneViewerIOS::Init(){

    }
    
    void SceneViewerIOS::Start(){
        tin::Engine::Create();
        
        tin::tinNSLoggerInterface *nsLoggerInterface = new tin::tinNSLoggerInterface();
        NSString *resourceDirectory = [[NSBundle mainBundle] resourcePath];
        tin::EngineOptions options;
        options.basepath = [resourceDirectory cStringUsingEncoding:NSUTF8StringEncoding];
        options.logger_interface = nsLoggerInterface;
        options.render_version = tin::kOpenGLES20;
        options.use_web_backend = true;
        options.run_in_background = true;
        tin::Engine::SharedInstance()->InitWithOptions(options);
        started = true;
    }
    
    void SceneViewerIOS::Shutdown(){
        
    }
    
    std::vector<std::string> SceneViewerIOS::SceneFilenames(){
        return tin::FileSystem::SharedInstance()->ListFilesOfType("tinScene");
    }
    
    void SceneViewerIOS::AddVirtualFileSystem(std::string file_name){
        tin::FileSystem::SharedInstance()->AddFileSystem(file_name, tin::VFSTypeArchive);
    }
    
    void SceneViewerIOS::AddVirtualFilePath(std::string path){
        tin::FileSystem::SharedInstance()->AddFileSystem(path, tin::VFSTypePath);
    }
    
    void OrbitCamera(Node *camera_node, float x, float y, float z){
//        glm::vec3 c_position = camera_node->transform().position();
//        glm::vec3 up = glm::vec3(0, 1, 0);
//        c_position = c_position - camera_node->camera()->center_of_interest();
//        float length = glm::distance(c_position, camera_node->camera()->center_of_interest());
//        length += z * 0.1f;
//        glm::vec3 x_axis = glm::cross(c_position, up);
//        
//        glm::vec3 norm_pos = c_position;
//        norm_pos = glm::normalize(norm_pos);
//        float dot_y = glm::dot(norm_pos, up);
//        
//        x_axis = glm::normalize(x_axis);
//        glm::quat pitch;
//        if (!(dot_y < 0.01 && y < 0) && !(dot_y > 0.8 && y > 0)){
//            pitch = glm::rotate(pitch, y * 0.01f, x_axis);
//        }
//        glm::quat heading;
//        heading = glm::rotate(heading, -x * 0.01f, up);
//        
//        c_position = glm::normalize(c_position);
//        c_position = pitch * heading * c_position;
//        c_position = c_position * length;
//        c_position = c_position + camera_node->camera()->center_of_interest();
//        
//        
//        camera_node->transform().position(c_position);
//        camera_node->transform().LookAt(camera_node->camera()->center_of_interest());
    }
    
    void SceneViewerIOS::Frame(){
        if (!started){
            Start();
        }
//        tin::Engine::SharedInstance()->DrawSingleFrame();
        // If we have one touch we use that to rotate the camera
//        Scene *current_scene = Session::SharedInstance()->CurrentScene();
//        if (current_scene != NULL){
//            Node *camera_node = current_scene->CurrentCameraNode();
//            if (camera_node != NULL){
//                if (InputManager::SharedInstance()->NumTouches() == 1){
//                    tin::Touch touch = InputManager::SharedInstance()->GetTouch(0);
//                    if (touch.phase == kTouchPhaseMoved){
//                        Vec2 delta;
//                        delta.x = touch.position.x - touch.previous_position.x;
//                        delta.y = touch.position.y - touch.previous_position.y;
//
//                        OrbitCamera(camera_node, delta.x, delta.y, 0.0f);
//                    }
//                } else if (InputManager::SharedInstance()->NumTouches() == 2){
//                    float delta_zoom = 0.0f;
//                    if (InputManager::SharedInstance()->PinchGesture().isPinching){
//                        delta_zoom = InputManager::SharedInstance()->PinchGesture().delta;
//                        OrbitCamera(camera_node, 0.0f, 0.0f, -delta_zoom);
//                    }
//                } else if (InputManager::SharedInstance()->Mouse().buttons[InputButtons::MouseButton0]){
//                    InputMouse mouse = InputManager::SharedInstance()->Mouse();
//                    OrbitCamera(camera_node, mouse.delta.x, mouse.delta.y, 0.0f);
//                }
//            }
//        }
    }
    
    void SceneViewerIOS::LoadScene(std::string filename){
        CommandSystem::SharedInstance()->QueueCommand("f_LoadWorld", filename);
    }
    
    void SceneViewerIOS::CloseScene(std::string filename){
        CommandSystem::SharedInstance()->QueueCommand("f_UnloadAllWorlds");
    }
    
    void SceneViewerIOS::SetViewPort(int x, int y, int width, int height){
        if (started){
            Engine::SharedInstance()->ReshapeRenderer(width, height);
        }
    }
}

