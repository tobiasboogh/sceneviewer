//
//  AppDelegate.h
//  EngineIOSBase
//
//  Created by Tobias Boogh on 12-07-27.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
