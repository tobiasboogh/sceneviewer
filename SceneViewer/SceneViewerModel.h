//
//  SceneViewerModel.h
//  SceneViewer
//
//  Created by Tobias Boogh on 9/21/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SceneViewerModel : NSObject

typedef void (^CompletionBlock)(BOOL success, NSArray *filenames);
/*! use getInstance to get a singleton instance of the Model*/
+(SceneViewerModel *)getInstance;
-(void)refresh:(CompletionBlock)successBlock;
-(void)unzipResourceNamed:(NSString *)name withCompletionBlock:(CompletionBlock)completion;
-(void)clearFiles;
@end
