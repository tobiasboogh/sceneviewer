//
//  SceneViewerModel.m
//  SceneViewer
//
//  Created by Tobias Boogh on 9/21/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import "SceneViewerModel.h"

@interface SceneViewerModel(){
    dispatch_queue_t _file_queue;
}
@end

@implementation SceneViewerModel

+(SceneViewerModel *)getInstance{
    static dispatch_once_t onceToken;
    static SceneViewerModel *sceneViewerModel;
    dispatch_once(&onceToken, ^{
        sceneViewerModel = [[SceneViewerModel alloc] init];
    });
    return sceneViewerModel;
}

- (id)init {
    self = [super init];
    if (self) {
        _file_queue = dispatch_queue_create("com.tincan.filemanager", nil);
//        NSString *documentDirectory = [@"~/tmp" stringByExpandingTildeInPath];
//        NSString *dataDirectory = [documentDirectory stringByAppendingPathComponent:@"data"];
//        NSFileManager *filemanager = [NSFileManager defaultManager];
//        NSError *error = nil;
//        if ([filemanager createDirectoryAtPath:dataDirectory withIntermediateDirectories:YES attributes:nil error:&error]){
//            NSLog(@"%@", [error description]);
//        }
    }
    return self;
}

-(void)refresh:(CompletionBlock)completionBlock{
    dispatch_async(_file_queue, ^{
//        NSFileManager *fileManager = [[NSFileManager alloc] init];
//        
//        NSString *documentFileDirectory = [@"~/Documents" stringByExpandingTildeInPath];
//        NSDirectoryEnumerator *dirEnumerator = [fileManager enumeratorAtPath:documentFileDirectory];
//        NSMutableArray *filenameArray = [[NSMutableArray alloc] init];
//        NSString *filename = nil;
//        while (filename = [dirEnumerator nextObject]){
//            if ([[filename pathExtension] isEqualToString:@"zip"]){
//                [filenameArray addObject:filename];
//            }
//        }
        
        NSArray *paths = [[NSBundle mainBundle] pathsForResourcesOfType:@"tinScene" inDirectory:@"scenes"];
        NSMutableArray *nicePaths = [[NSMutableArray alloc] initWithCapacity:1];
        for (NSString *path in paths){
            [nicePaths addObject:[path lastPathComponent]];
        }
        [nicePaths addObject:@"testEnvironmentMapping.tinScene"];
        if (completionBlock){
            dispatch_sync(dispatch_get_main_queue() , ^{
                completionBlock(YES, nicePaths);
            });
            
        }
    });
}

-(void)unzipResourceNamed:(NSString *)name withCompletionBlock:(CompletionBlock)completion{
//    NSString *documentDirectory = [@"~/Documents" stringByExpandingTildeInPath];
////    NSString *tempDirectory = [@"~/tmp" stringByExpandingTildeInPath];
////    NSString *dataDirectory = [tempDirectory stringByAppendingPathComponent:@"data"];
//    NSString *filePath = [documentDirectory stringByAppendingPathComponent:name];
//    
//    /* try this async, edit SSZipArchive along the way to support GCD */
//    
//    dispatch_async(_file_queue, ^{
//        [self clearFiles];
//        // first we remove the old files in the directory
//        NSFileManager *filemanager = [[NSFileManager alloc] init];
//        BOOL success = [SSZipArchive unzipFileAtPath:filePath toDestination:documentDirectory];
//        
//        NSMutableArray *scenesExtracted = [[NSMutableArray alloc] initWithCapacity:1];
//        NSDirectoryEnumerator *fileEnumerator = [filemanager enumeratorAtPath:documentDirectory];
//        NSString *filename = nil;
//        while (filename = [fileEnumerator nextObject]){
//            BOOL skipFile = NO;
//            NSArray *components = [filename pathComponents];
//            for (NSString *string in components){
//                if ([string isEqualToString:@"__MACOSX"]){
//                    skipFile = YES;
//                }
//            }
//            if ([[filename pathExtension] isEqualToString:@"tinScene"] && !skipFile){
//                [scenesExtracted addObject:filename];
//            }
//        }
//        
//        if (completion){
//            completion(success, scenesExtracted);
//        }
//    });
}

-(void)clearFiles{
/*    NSString *tempDirectory = [@"~/tmp" stringByExpandingTildeInPath];
    NSString *dataDirectory = [tempDirectory stringByAppendingPathComponent:@"data"];
    
    NSDirectoryEnumerator *fileEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:dataDirectory];
    NSString *filename = nil;
    while (filename = [fileEnumerator nextObject]){
        // If the file is not a shader we can remove it
        if (![[filename pathExtension] isEqualToString:@"vsh"] && ![[filename pathExtension] isEqualToString:@"fsh"]){
            NSString *removeFilePath = [dataDirectory stringByAppendingPathComponent:filename];
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtPath:removeFilePath error:&error];
            if (error){
                NSLog(@"%@", [error description]);
            }
        }
    }*/

}

@end
