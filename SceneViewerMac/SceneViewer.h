//
//  SceneViewer.h
//  Game
//
//  Created by Tobias Boogh on 12-09-06.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#ifndef _SceneViewer_H_
#define _SceneViewer_H_

#include <Engine/Application.h>
#include <string>
#include <vector>
namespace tin{
    class SceneViewer : public Application{
        public :
            ~SceneViewer();
            void Init();
            void Start();
            void Shutdown();
        
            void Frame();

            void LoadScene(std::string filename);
            void CloseScene(std::string filename);
        
            void SetViewPort(int x, int y, int width, int height);
        
            std::vector<std::string> SceneFilenames();
            void AddVirtualFileSystem(std::string file_name);
            void AddVirtualFilePath(std::string path);
        
        private:
            bool started = false;
    };
}
#endif
