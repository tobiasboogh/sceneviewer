//
//  AppDelegate.h
//  SceneViewerMac
//
//  Created by Tobias Boogh on 11/10/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
