//
//  tinNSLoggerInterface.cpp
//  EngineMacTest
//
//  Created by Tobias Boogh on 4/18/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//

#include "tinNSLoggerInterface.h"
extern void LogNSMessage(const char *message);
extern void LogNSWarning(const char *message);
extern void LogNSError(const char *message);
namespace tin {
    void tinNSLoggerInterface::LogMessage(const char *message){
        LogNSMessage(message);
    }
    
    void tinNSLoggerInterface::LogWarning(const char *message){
        LogNSWarning(message);
    }
    
    void tinNSLoggerInterface::LogError(const char *message){
        LogNSError(message);
    }
    
    LoggerOption tinNSLoggerInterface::RespondsToOptions(){
        return (MESSAGE | WARNING | ERROR | VERBOSE);
    }
}