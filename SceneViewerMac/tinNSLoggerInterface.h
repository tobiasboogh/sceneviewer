//
//  tinNSLoggerInterface.h
//  EngineMacTest
//
//  Created by Tobias Boogh on 4/18/12.
//  Copyright (c) 2012 t.inc.an. All rights reserved.
//

#ifndef _tinNSLoggerInterface_H_
#define _tinNSLoggerInterface_H_
#include <Engine/LoggerInterface.h>

namespace tin{
    class tinNSLoggerInterface : public LoggerInterface{
        public:
            void LogMessage(const char *message);
            void LogWarning(const char *message);
            void LogError(const char *message);
            
            LoggerOption RespondsToOptions();
    };
} // tin
#endif
