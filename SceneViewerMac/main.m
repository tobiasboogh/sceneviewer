//
//  main.m
//  SceneViewerMac
//
//  Created by Tobias Boogh on 11/10/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
