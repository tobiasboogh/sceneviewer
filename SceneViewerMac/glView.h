//
//  glView.h
//  SceneViewer
//
//  Created by Tobias Boogh on 11/18/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>
#import "EngineObject.h"
@interface glView : NSOpenGLView  {
    CVDisplayLinkRef displayLink;
    double    deltaTime;
    double    outputTime;
    float    viewWidth;
    float    viewHeight;
}
@property (nonatomic, weak) IBOutlet EngineObject *engineObject;
@property (nonatomic, strong) NSMutableArray *inputArray;
@end
