//
//  AppDelegate.m
//  SceneViewerMac
//
//  Created by Tobias Boogh on 11/10/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

@end
