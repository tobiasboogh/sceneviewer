//
//  EngineObject.m
//  SceneViewer
//
//  Created by Tobias Boogh on 11/18/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import "EngineObject.h"
#include "tinNSLoggerInterface.h"
#include <vector>
#include <string>

@implementation EngineObject

-(void)awakeFromNib{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shutDown) name:NSApplicationWillTerminateNotification object:nil];
    sceneViewer = new tin::SceneViewer();
}

-(void)shutDown{
    sceneViewer->Shutdown();
}
     
-(void)startEngine{
    sceneViewer->Start();
}

-(tin::SceneViewer *)sceneViewer{
    return sceneViewer;
}

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{
    return self.sceneFiles.count;
}

-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    return [self.sceneFiles objectAtIndex:row];
}

-(void)tableViewSelectionDidChange:(NSNotification *)notification{
    NSString *sceneFile = [self.sceneFiles objectAtIndex:self.tableView.selectedRow];
    NSLog(@"Open: %@", sceneFile);
    sceneViewer->LoadScene([sceneFile cStringUsingEncoding:NSUTF8StringEncoding]);
}

-(IBAction)reloadScenes:(id)sender{
    NSMutableArray *file_array = [[NSMutableArray alloc] init];
    std::vector<std::string> scene_files = sceneViewer->SceneFilenames();
    for (auto &filename : scene_files){
        [file_array addObject:[NSString stringWithUTF8String:filename.c_str()]];
    }
    self.sceneFiles = file_array;
    [self.tableView reloadData];
}

-(IBAction)closeAllScene:(id)sender{
    sceneViewer->CloseScene("A scene");
}

-(void)addResource:(id)sender{
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    openPanel.allowsMultipleSelection = NO;
    openPanel.canChooseDirectories = YES;
    openPanel.canChooseFiles = YES;
    openPanel.allowedFileTypes = @[@"tinPak"];
    [openPanel runModal];
    
    for (NSURL *url in openPanel.URLs){
        NSLog(@"%@", url);
        NSString *path = [url path];
        NSString *extension = [path pathExtension];
        if ([extension isEqualToString:@"tinPak"]){
            sceneViewer->AddVirtualFileSystem([path cStringUsingEncoding:NSUTF8StringEncoding]);
        } else {
            sceneViewer->AddVirtualFilePath([path cStringUsingEncoding:NSUTF8StringEncoding]);
        }
    }
    [self reloadScenes:self];
}


@end
