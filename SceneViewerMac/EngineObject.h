//
//  EngineObject.h
//  SceneViewer
//
//  Created by Tobias Boogh on 11/18/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "SceneViewer.h"
@interface EngineObject : NSObject <NSTableViewDataSource, NSTableViewDelegate, NSOpenSavePanelDelegate>{
    tin::SceneViewer *sceneViewer;
}
@property (nonatomic, strong) NSArray *sceneFiles;
@property (nonatomic, weak) IBOutlet NSTableView *tableView;
-(IBAction)reloadScenes:(id)sender;
-(IBAction)closeAllScene:(id)sender;
-(IBAction)addResource:(id)sender;
-(tin::SceneViewer *)sceneViewer;
-(void)startEngine;
@end
