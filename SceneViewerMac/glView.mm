//
//  glView.m
//  SceneViewer
//
//  Created by Tobias Boogh on 11/18/12.
//  Copyright (c) 2012 Tobias Boogh. All rights reserved.
//

#import "glView.h"
#include <Engine/InputTypes.h>
#include "tinNSLoggerInterface.h"
@interface glView(){
    bool started;
}
- (CVReturn)getFrameForTime:(const CVTimeStamp *)outputTime;
- (void)drawFrame;
-(void)prepareOpenGL;
@end

@implementation glView

glView *gl_view;

#pragma mark -
#pragma mark Display Link

static CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *now,
                                      const CVTimeStamp *outputTime, CVOptionFlags flagsIn,
                                      CVOptionFlags *flagsOut, void *displayLinkContext){
    // go back to Obj-C for easy access to instance variables
    CVReturn result = [(__bridge glView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}

- (CVReturn)getFrameForTime:(const CVTimeStamp *)outputTime
{
    // deltaTime is unused in this bare bones demo, but here's how to calculate it using display link info
    deltaTime = 1.0 / (outputTime->rateScalar * (double)outputTime->videoTimeScale / (double)outputTime->videoRefreshPeriod);
    
    [self drawFrame];
    
    return kCVReturnSuccess;
}

- (void)dealloc
{
    CVDisplayLinkRelease(displayLink);
}

-(void)prepareOpenGL {
    started = NO;
    self.inputArray = [[NSMutableArray alloc] initWithCapacity:1];
    // set synch to VBL to eliminate tearing
    GLint    vblSynch = 1;
    NSOpenGLPixelFormatAttribute attr[] = { kCGLPFAOpenGLProfile, kCGLOGLPVersion_3_2_Core, 
                                            kCGLPFAAccelerated,
                                            kCGLPFAColorSize, 32,
                                            kCGLPFADepthSize, 16,
                                            kCGLPFADoubleBuffer,
                                            kCGLPFASampleBuffers, 1,
                                            kCGLPFASamples, 6,
                                            kCGLPFANoRecovery,
                                            0 };
    NSOpenGLPixelFormat *format = [[NSOpenGLPixelFormat alloc] initWithAttributes:attr];
    [self setPixelFormat:format];
    
    [[self openGLContext] setValues:&vblSynch forParameter:NSOpenGLCPSwapInterval];
    // set up the display link

    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, (__bridge void *)(self));
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    
    // Precook the OpenGL View
//    [self drawFrame];
//    [self drawFrame];
//    [self drawFrame];
    
    [[self window] setAcceptsMouseMovedEvents:YES];
    
    [NSEvent addLocalMonitorForEventsMatchingMask:NSAnyEventMask handler:^NSEvent *(NSEvent *event) {
        // We only process the event if it is in the active area
        NSPoint viewPoint = [self convertPoint:[[self window] convertScreenToBase:[NSEvent mouseLocation]] fromView:nil];
        if (![self mouse:viewPoint inRect:self.bounds]){
            tin::InputInfo input_info;
            input_info.type = tin::kInputTypeClear;
            [self.inputArray addObject:[NSValue value:&input_info withObjCType:@encode(tin::InputInfo)]];
        } else {
            switch (event.type) {
                case NSLeftMouseDown:
                case NSRightMouseDown:
                case NSLeftMouseUp:
                case NSRightMouseUp:
                case NSMouseMoved:
                case NSLeftMouseDragged:
                case NSRightMouseDragged:{
                    [self handleMouseEvent:event];
                } break;
                case NSKeyDown:
                case NSKeyUp:{
    //                NSLog(@"%@", event);
                } break;
                default:{
    //                NSLog(@"%@", event);
                } break;
            }
        }
        return event;
    }];
}

-(void)handleMouseEvent:(NSEvent *)event{
    tin::InputInfo input_info;
    switch (event.type){
        case NSLeftMouseDown: {
            NSPoint viewPoint = [self convertPoint:[[self window] convertScreenToBase:[NSEvent mouseLocation]] fromView:nil];
            if (![self mouse:viewPoint inRect:self.bounds]){
                return;
            }
            input_info.type = tin::kInputTypeButton;
            input_info.input_index = tin::InputButtons::MouseButton0;
            input_info.value1 = 1.0;
        } break;
        case NSRightMouseDown: {
            input_info.type = tin::kInputTypeButton;
            input_info.input_index = tin::InputButtons::MouseButton1;
            input_info.value1 = 1.0;
        } break;
        case NSLeftMouseUp: {
            input_info.type = tin::kInputTypeButton;
            input_info.input_index = tin::InputButtons::MouseButton0;
            input_info.value1 = 0.0;
        } break;
        case NSRightMouseUp: {
            input_info.type = tin::kInputTypeButton;
            input_info.input_index = tin::InputButtons::MouseButton1;
            input_info.value1 = 0.0;
        } break;
        case NSMouseMoved:
        case NSLeftMouseDragged:
        case NSRightMouseDragged:{
            NSPoint viewPoint = [self convertPoint:[[self window] convertScreenToBase:[NSEvent mouseLocation]] fromView:nil];
            input_info.type = tin::kInputTypeAxis;
            input_info.input_index = tin::InputAxis::MouseAxis;
            
            input_info.position.x = viewPoint.x;
            input_info.position.y = viewPoint.y;
            input_info.delta.x    = event.deltaX;
            input_info.delta.y    = event.deltaY;
        } break;
    }
    [self.inputArray addObject:[NSValue value:&input_info withObjCType:@encode(tin::InputInfo)]];
}

- (void)awakeFromNib
{
    NSSize    viewBounds = [self bounds].size;
    viewWidth = viewBounds.width;
    viewHeight = viewBounds.height;
    [self prepareOpenGL];
    // activate the display link
    CVDisplayLinkStart(displayLink);
    gl_view = self;
}

- (void)reshape
{
    NSSize    viewBounds = [self bounds].size;
    viewWidth = viewBounds.width;
    viewHeight = viewBounds.height;
    
    NSOpenGLContext    *currentContext = [self openGLContext];
    [currentContext makeCurrentContext];
    
    // remember to lock the context before we touch it since display link is threaded
    CGLLockContext((CGLContextObj)[currentContext CGLContextObj]);
    tin::SceneViewer *sceneViewer = [_engineObject sceneViewer];
    sceneViewer->SetViewPort(0, 0, viewWidth, viewHeight);
    // let the context know we've changed size
    [[self openGLContext] update];
    
    CGLUnlockContext((CGLContextObj)[currentContext CGLContextObj]);
}

- (void)drawRect:(NSRect)rect
{
    [self drawFrame];
}

void MakeCurrentContextActive(){
    [gl_view setCurrentContext];
}


-(void)setCurrentContext{
    if ([NSThread isMainThread]){
        NSLog(@"halt");
    }
    NSOpenGLContext    *currentContext = [self openGLContext];
    [currentContext makeCurrentContext];
}

- (void)drawFrame {
    if ([NSThread isMainThread]){
        
        NSOpenGLContext    *currentContext = [self openGLContext];
        [currentContext makeCurrentContext];
        
        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status == GL_FRAMEBUFFER_COMPLETE){
            [_engineObject sceneViewer]->Frame();
        } else {
            [_engineObject startEngine];
        }
        [currentContext flushBuffer];
    } else {
//        [[NSThread mainThread] performSelector:@selector(drawFrame)];
        [self performSelectorOnMainThread:@selector(drawFrame) withObject:nil waitUntilDone:YES];
    }
}

void LogNSMessage(const char *message){
    NSLog(@"%s", message);
}

void LogNSWarning(const char *message){
    NSLog(@"%s", message);
}

void LogNSError(const char *message){
    NSLog(@"%s", message);
}

tin::InputInfo tin::GetBufferedInput(){
    __block tin::InputInfo input_info;
//    dispatch_sync(dispatch_get_main_queue(), ^{
//        NSLog(@"%ld", gl_view.inputArray.count);
        if ([gl_view.inputArray count] == 0){
            input_info.type = tin::kInputTypeEnd;
        } else {
            NSValue *value = [gl_view.inputArray objectAtIndex:0];
            [value getValue:&input_info];
            [gl_view.inputArray removeObjectAtIndex:0];
        }
//    });
    return input_info;
}

- (BOOL)acceptsFirstResponder {
    return YES;
}

@end
